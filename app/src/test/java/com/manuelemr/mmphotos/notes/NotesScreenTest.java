package com.manuelemr.mmphotos.notes;

import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.presentation.modules.notes.NotesFragment;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.support.v4.SupportFragmentTestUtil;

import static junit.framework.TestCase.assertNotNull;

/**
 * Created by manuelmunoz on 2/15/17.
 */

@Config(packageName = "com.manuelemr.mmphotos", manifest = "src/main/AndroidManifest.xml")
@RunWith(RobolectricTestRunner.class)
public class NotesScreenTest {

    @Test
    public void clickingFab_OpenDialog(){

        NotesFragment notesFragment = new NotesFragment();
        SupportFragmentTestUtil.startFragment(notesFragment);
        assertNotNull(notesFragment);

        notesFragment.getView().findViewById(R.id.fab_addnote).performClick();

        assertNotNull(notesFragment.getActivity().findViewById(R.id.tv_title));

//        Dialog dialog = ShadowDialog.getLatestDialog();
//        CreateNoteDialog createNoteDialog = Shadows.shadowOf(dialog);
    }
}
