package com.manuelemr.mmphotos.notes;

import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.modules.notes.GetNotesInteractor;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.presentation.modules.notes.NotesPresenter;
import com.manuelemr.mmphotos.presentation.modules.notes.NotesView;
import com.manuelemr.mmphotos.data.services.NotesService;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.AddNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.LoadingNotesViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;
import com.manuelemr.mmphotos.presentation.navigation.AddNoteNavigator;
import com.manuelemr.mmphotos.presentation.navigation.NoteDetailNavigator;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class NotesPresenterTest {

    private static final Note NOTE = new Note("test_title", "test_description");
    private static final List<Note> NOTES = new ArrayList<Note>() {
        {
            add(new Note("test_title_1", "test_description_1"));
            add(new Note("test_title_2", "test_description_2"));
            add(new Note("test_title_3", "test_description_3"));
            add(new Note("test_title_4", "test_description_4"));
            add(new Note("test_title_5", "test_description_5"));
            add(new Note("test_title_6", "test_description_6"));
        }
    };

    @Mock
    private NotesService mNotesService;
    @Mock
    private NotesView mNotesView;
    @Mock
    private GetNotesInteractor mGetNotesInteractor;
    @Mock
    private AddNoteNavigator mAddNoteNavigator;
    @Mock
    private NoteDetailNavigator mNoteDetailNavigator;
    @Mock
    private ExceptionParser mExceptionParser;

    private NotesPresenter mNotesPresenter;

    private PublishSubject<Boolean> mLoadNotesTestObservable;
    private PublishSubject<Object> mAddNoteTestObservable;
    private PublishSubject<Note> mToNoteTestObservable;
    private PublishSubject<NotesViewState> mFromNoteDetailTestObservable;
    private PublishSubject<List<Note>> mLoadedNotesTestObservable;
    private PublishSubject<NotesViewState> mAddedNoteTestObservable;

    @Before
    public void setupNotesPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        initSubjects();
        setupResponses();

        // Get a reference to the class under test
        mNotesPresenter = new NotesPresenter(
                new LoadingNotesViewState(),
                new ObserveOn() {
                    @Override
                    public Scheduler getScheduler() {
                        return Schedulers.trampoline();
                    }
                },
                mGetNotesInteractor,
                mAddNoteNavigator,
                mNoteDetailNavigator,
                mExceptionParser
        );
        mNotesPresenter.attach(mNotesView);
    }

    private void initSubjects() {

        mLoadNotesTestObservable = PublishSubject.create();
        mAddNoteTestObservable = PublishSubject.create();
        mToNoteTestObservable = PublishSubject.create();
        mFromNoteDetailTestObservable = PublishSubject.create();
        mLoadedNotesTestObservable = PublishSubject.create();
        mAddedNoteTestObservable = PublishSubject.create();
    }

    private void setupResponses() {

        when(mNotesView.loadAllNotes()).thenReturn(mLoadNotesTestObservable);
        when(mNotesView.addNote()).thenReturn(mAddNoteTestObservable);
        when(mNotesView.toNoteDetail()).thenReturn(mToNoteTestObservable);
        when(mAddNoteNavigator.navigate()).thenReturn(mFromNoteDetailTestObservable);
        when(mGetNotesInteractor.request()).thenReturn(mLoadedNotesTestObservable);
        when(mAddNoteNavigator.navigate()).thenReturn(mAddedNoteTestObservable);
    }


    @Test
    public void clickOnFab_ShowsAddNoteUI(){

        mAddNoteTestObservable.onNext(new Object());

        verify(mAddNoteNavigator).navigate();
    }

    @Test
    public void clickOnNote_ShowNoteDetailUI(){

        mToNoteTestObservable.onNext(NOTE);

        verify(mNoteDetailNavigator).navigate(NOTE);
    }

    @Test
    public void loadView_LoadingInitialState(){

        mLoadNotesTestObservable.onNext(true);

        ArgumentCaptor<NotesViewState> captor1 = ArgumentCaptor.forClass(NotesViewState.class);

        verify(mNotesView).render(captor1.capture());
        //Check loading state is set
        assertEquals(true, captor1.getValue().isLoading());

        ArgumentCaptor<NotesViewState> captor2 = ArgumentCaptor.forClass(NotesViewState.class);
        mLoadedNotesTestObservable.onNext(NOTES);
        verify(mNotesView, atMost(2)).render(captor2.capture());

        //check notes list was returned, and is loading is false
        assertEquals(NOTES, captor2.getValue().getNoteList());
        assertEquals(false, captor2.getValue().isLoading());
    }

    @Test
    public void clickAddNote_returnsNote() {

        NotesViewState newNoteState = new AddNoteViewState(NOTE);
        mAddNoteTestObservable.onNext(new Object());
        mAddedNoteTestObservable.onNext(newNoteState);

        ArgumentCaptor<NotesViewState> captor = ArgumentCaptor.forClass(NotesViewState.class);

        //Two calls because LoadingNotesState is always set
        verify(mNotesView, atMost(2)).render(captor.capture());

        assertEquals(NOTE, NOTE);
    }

    @Test
    public void loadNotesFromServiceAndView(){

        Note note1 = new Note("TEST", "TEST1");
        Note note2 = new Note("TEST", "TEST2");
        Note note3 = new Note("TEST", "TEST3");

        List<Note> noteArray = new ArrayList<Note>() {{ add(note1); add(note2); add(note3); }};
        Observable<List<Note>> testObservable = Observable.just(noteArray);

        TestObserver<List<Note>> testSubscriber = new TestObserver<>();

        when(mNotesService.getAllNotes()).thenReturn(testObservable);
        when(mGetNotesInteractor.request()).thenReturn(testObservable);

        testObservable.subscribe(testSubscriber);

        mNotesPresenter.attach(mNotesView);

//        verify(mNotesView).showLoader(true);
        verify(mGetNotesInteractor).request();

        testSubscriber.awaitTerminalEvent();
        testSubscriber.assertComplete();

//        verify(mNotesView).showLoader(false);
//        verify(mNotesView).showNotes(eq(noteArray));
    }
}


