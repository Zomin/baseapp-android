package com.manuelemr.mmphotos.addnote;

import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.modules.addnote.AddNoteInteractor;
import com.manuelemr.mmphotos.presentation.modules.addnote.AddNotePresenter;
import com.manuelemr.mmphotos.presentation.modules.addnote.AddNoteView;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOn;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public class AddNotePresenterTest {

    @Mock
    AddNotePresenter mAddNotePresenter;
    @Mock
    AddNoteView mAddNoteView;
    @Mock
    AddNoteInteractor mAddNoteInteractor;
    @Mock
    ExceptionParser mExceptionParser;

    @Before
    public void setupAddNotePresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mAddNotePresenter = new AddNotePresenter(
                new ObserveOn() {
                    @Override
                    public Scheduler getScheduler() {
                        return Schedulers.trampoline();
                    }
                },
                mAddNoteInteractor,
                mExceptionParser
        );
        mAddNotePresenter.attach(mAddNoteView);
    }

    @Test
    public void clickOK_FullNote(){

        Note testNote = new Note("TEST", "TEST");

        callAddNote(testNote);

        assertFalse("Note has no empty fields", mAddNotePresenter.noteHasEmptyFields(testNote));

        verify(mAddNoteInteractor).setNote(testNote);
        verify(mAddNoteInteractor).request();

//        verify(mAddNoteView).showNote(testNote);
    }

    @Test
    public void clickOK_EmptyTitleNote() {

        Note emptyNote = new Note("", "TEST");

        when(mExceptionParser.toMessage(any())).thenReturn("Error test");
        callAddNote(emptyNote);

//        verify(mAddNoteView).showError(eq("Err"));
    }

    @Test
    public void clickOK_EmptyContentNote() {

        Note emptyNote = new Note("TEST", "");

        when(mExceptionParser.toMessage(any())).thenReturn("Error test");
        callAddNote(emptyNote);

//        verify(mAddNoteView).showError(eq("Err"));
    }

    private void callAddNote(Note note){

//        when(mAddNoteInteractor.request()).thenReturn(Observable.just(true));
//
//        mAddNotePresenter.addNote(note);
    }
}
