package com.manuelemr.mmphotos.notedetail;

import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.modules.favoritenote.FavoriteNoteInteractor;
import com.manuelemr.mmphotos.data.modules.notedetail.GetNoteInteractor;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.presentation.modules.notedetail.NoteDetailPresenter;
import com.manuelemr.mmphotos.presentation.modules.notedetail.NoteDetailView;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

import static org.mockito.Mockito.*;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public class NoteDetailPresenterTest {

    @Mock
    GetNoteInteractor mGetNoteInteractor;
    @Mock
    FavoriteNoteInteractor mFavoriteNoteInteractor;
    @Mock
    ExceptionParser mExceptionParser;
    @Mock
    NoteDetailView mNoteDetailView;

    NoteDetailPresenter mNoteDetailPresenter;

    @Before
    public void init(){

        MockitoAnnotations.initMocks(this);

        mNoteDetailPresenter = new NoteDetailPresenter(
                new ObserveOn() {
                    @Override
                    public Scheduler getScheduler() {
                        return Schedulers.trampoline();
                    }
                },
                mGetNoteInteractor,
                mFavoriteNoteInteractor,
                mExceptionParser
        );

        mNoteDetailPresenter.attach(mNoteDetailView);
    }

    @Test
    public void loadNoteAndShow(){

        Note note = new Note("TEST", "TEST");
        int noteId = 100;
        note.setId(noteId);

//        TestSubscriber<Note> testSubscriber = new TestSubscriber<>();
//        Observable<Note> testObservable = Observable.just(note);
//        testObservable.subscribe(testSubscriber);
//
//
//        when(mGetNoteInteractor.request()).thenReturn(testObservable);
//
//        mNoteDetailPresenter.findNoteWithId(noteId);
//
//        verify(mGetNoteInteractor).setNoteId(noteId);
//        verify(mGetNoteInteractor).request();
//
//        testSubscriber.awaitTerminalEvent();
//        verify(mNoteDetailView).showNote(eq(note));
    }
}
