package com.manuelemr.mmphotos.presentation.modules.notedetail;

import com.manuelemr.mmphotos.data.base.BaseView;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadNoteViewState;

import io.reactivex.Observable;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public interface NoteDetailView extends BaseView {

    Observable<Integer> loadNote();
    Observable<Integer> favoriteNote();

    void render(LoadNoteViewState state);
}
