package com.manuelemr.mmphotos.presentation.modules.notedetail;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.BaseFragment;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadNoteViewState;

import butterknife.BindView;
import io.reactivex.Observable;

/**
 * A simple {@link Fragment} subclass.
 */
public class NoteDetailFragment extends BaseFragment<NoteDetailPresenter> implements NoteDetailView {

    public static final String NOTE_ID = "note_id";

    @BindView(R.id.tv_title)
    TextView mTitleTextView;
    @BindView(R.id.tv_content)
    TextView mContentTextView;
    @BindView(R.id.fab_favorite)
    FloatingActionButton mFavoriteActionButton;

    private int mNoteId;

    public NoteDetailFragment() {
        // Required empty public constructor
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_note_detail;
    }

    @Override
    protected void viewCreated(View view, Bundle savedInstanceState) {

        mNoteId = getArguments().getInt(NOTE_ID);
    }

    @Override
    protected void injectDagger() {
        getApplication().getAppComponent().inject(this);
    }

    private void showNote(Note note) {

        mTitleTextView.setText(note.getTitle());
        mContentTextView.setText(note.getContent());
        mFavoriteActionButton.setImageResource(note.isFavorite() ?
                R.drawable.ic_favorite_black_24dp : R.drawable.ic_favorite_border_black_24dp);
    }
    /** Note detail view implementation **/

    @Override
    public Observable<Integer> loadNote() {
        return Observable.create(subscriber -> {
            subscriber.onNext(mNoteId);
        });
    }

    @Override
    public Observable<Integer> favoriteNote() {
        return RxView.clicks(mFavoriteActionButton)
                .flatMap(ignored -> Observable.just(mNoteId));
    }

    @Override
    public void render(LoadNoteViewState state) {

        if(state.isLoading()) showLoaderIndeterminate();
        else hideLoader();

        if(state.getLoadedNote() != null){
            showNote(state.getLoadedNote());
        } else if(state.getError() != null) {
            showDialog(getString(R.string.error), state.getError());
        }
    }

    /** End **/
}
