package com.manuelemr.mmphotos.presentation.modules.notes.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuelmunoz on 7/18/17.
 */

public class AddNoteViewState extends NotesViewState {

    public AddNoteViewState(Note newNote) {
        super(false, new ArrayList<>(), null, newNote, true, null);
    }
}
