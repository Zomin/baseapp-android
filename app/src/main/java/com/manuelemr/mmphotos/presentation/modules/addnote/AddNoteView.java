package com.manuelemr.mmphotos.presentation.modules.addnote;

import com.manuelemr.mmphotos.data.base.BaseView;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import io.reactivex.Observable;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public interface AddNoteView extends BaseView {

    Observable<Note> createNote();
    Observable<Boolean> dismissDialog();

    void render(NotesViewState state);
}
