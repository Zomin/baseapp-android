package com.manuelemr.mmphotos.presentation.navigation;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;

import com.manuelemr.mmphotos.presentation.base.BaseApplication;
import com.manuelemr.mmphotos.presentation.base.SingleFragmentActivity;

import javax.inject.Inject;

/**
 * Created by manuelmunoz on 7/18/17.
 */

public class Navigator {

    protected final BaseApplication mApplication;

    @Inject
    public Navigator(BaseApplication application) {
        mApplication = application;
    }

    public void pop() {
        Activity liveActivity = mApplication.getLiveActivity();
        if(liveActivity instanceof SingleFragmentActivity){
            ((SingleFragmentActivity)liveActivity).popBackStack();
        }
    }

    protected AppCompatActivity getLiveActivity() {
        AppCompatActivity liveActivity = mApplication.getLiveActivity();
        if(liveActivity == null){
            throw  new NullPointerException("Live Activity was null");
        }
        return liveActivity;
    }
}
