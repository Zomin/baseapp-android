package com.manuelemr.mmphotos.presentation.modules.notes;


import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jakewharton.rxbinding2.support.v4.widget.RxSwipeRefreshLayout;
import com.jakewharton.rxbinding2.view.RxView;
import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.BaseFragment;
import com.manuelemr.mmphotos.presentation.base.BaseRecyclerAdapter;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotesFragment extends BaseFragment<NotesPresenter> implements NotesView {

    @BindView(R.id.srl_notes)
    SwipeRefreshLayout mSwipeRefreshLayout;
    @BindView(R.id.rv_notes)
    RecyclerView mNotesRecyclerView;
    @BindView(R.id.fab_addnote)
    FloatingActionButton mAddNoteButton;

    BehaviorSubject<Boolean> mLoadNotesSubject;
    BehaviorSubject<Note> mNoteDetailSubject;

    private BaseRecyclerAdapter<Note, NoteViewHolder> mNoteRecyclerAdapter;

    public NotesFragment() {
        // Required empty public constructor
    }

    @Override
    public void viewCreated(View view,
                             Bundle savedInstanceState) {

        setupRecyclerView();
        setupObservables();
        init();
    }

    @Override
    protected void injectDagger() {
        getApplication().getAppComponent().inject(this);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_notes;
    }

    private void setupRecyclerView(){

        mNotesRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mNoteRecyclerAdapter = new BaseRecyclerAdapter<Note, NoteViewHolder>() {

            @Override
            public NoteViewHolder createViewHolder(ViewGroup parent, LayoutInflater layoutInflater) {

                View view = layoutInflater.inflate(R.layout.item_note, parent, false);
                return new NoteViewHolder(view);
            }
        };

        mNotesRecyclerView.setAdapter(mNoteRecyclerAdapter);
        mNoteRecyclerAdapter.setOnClickListener(
                ((item, viewHolder, position) -> mNoteDetailSubject.onNext(item)));
    }

    private void setupObservables() {

        mLoadNotesSubject = BehaviorSubject.create();
        mNoteDetailSubject = BehaviorSubject.create();

        //Load notes again when refreshing
        RxSwipeRefreshLayout
                .refreshes(mSwipeRefreshLayout)
                .subscribe(ignored -> mLoadNotesSubject.onNext(true));
    }

    private void init() {

        //Ask for items
        mLoadNotesSubject.onNext(true);
    }

    /** Notes View Implementation **/

    @Override
    public Observable<Boolean> loadAllNotes() {
        return mLoadNotesSubject;
    }

    @Override
    public Observable<Object> addNote() {
        return RxView.clicks(mAddNoteButton);
    }

    @Override
    public Observable<Note> toNoteDetail() {
        return mNoteDetailSubject;
    }

    @Override
    public void render(NotesViewState viewState) {

        if(viewState.isLoading()) mSwipeRefreshLayout.setRefreshing(true);
        else mSwipeRefreshLayout.setRefreshing(false);

        if(viewState.isNewNoteAdded() && viewState.getNewNote() != null){
            mNoteRecyclerAdapter.addItem(viewState.getNewNote());
        } else if(viewState.getNoteList() != null && viewState.getNoteList() != mNoteRecyclerAdapter.getItems()){
            mNoteRecyclerAdapter.setItems(viewState.getNoteList());
        } else if(viewState.getLoadingNotesError() != null) {
            showDialog(getString(R.string.error), viewState.getLoadingNotesError());
        }

    }

    /** End **/
}
