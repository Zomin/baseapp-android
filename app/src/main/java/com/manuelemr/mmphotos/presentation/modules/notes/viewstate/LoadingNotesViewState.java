package com.manuelemr.mmphotos.presentation.modules.notes.viewstate;

import java.util.ArrayList;

/**
 * Created by manuelmunoz on 7/18/17.
 */

public class LoadingNotesViewState extends NotesViewState {

    public LoadingNotesViewState() {
        super(true, new ArrayList<>(), null, null, false, null);
    }
}
