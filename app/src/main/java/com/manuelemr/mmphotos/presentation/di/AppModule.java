package com.manuelemr.mmphotos.presentation.di;

import com.manuelemr.mmphotos.presentation.base.BaseApplication;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuelmunoz on 2/15/17.
 */

@Module
public class AppModule {

    private final BaseApplication mBaseApplication;

    public AppModule(BaseApplication baseApplication){
        mBaseApplication = baseApplication;
    }

    @Provides
    @Singleton
    public BaseApplication provideBaseApplication() {
        return mBaseApplication;
    }
}
