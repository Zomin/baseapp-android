package com.manuelemr.mmphotos.presentation.navigation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.BaseApplication;
import com.manuelemr.mmphotos.presentation.base.SingleFragmentActivity;
import com.manuelemr.mmphotos.presentation.modules.notedetail.NoteDetailFragment;

/**
 * Created by manuelmunoz on 7/19/17.
 */

public class NoteDetailNavigator extends Navigator {
    public NoteDetailNavigator(BaseApplication application) {
        super(application);
    }

    public void navigate(Note note) {

        Bundle fragmentBundle = new Bundle();
        fragmentBundle.putInt(NoteDetailFragment.NOTE_ID, note.getId());

        Bundle activityBundle = new Bundle();
        activityBundle.putString(SingleFragmentActivity.Specifics.TITLE_KEY, mApplication.getString(R.string.title_activity_note_detail));
        activityBundle.putSerializable(SingleFragmentActivity.Specifics.FRAGMENT_KEY, NoteDetailFragment.class);
        activityBundle.putBundle(SingleFragmentActivity.Specifics.FRAGMENT_BUNDLE_KEY, fragmentBundle);

        Intent intent = new Intent(mApplication.getApplicationContext(), SingleFragmentActivity.class);
        intent.putExtras(activityBundle);

        Activity liveActivity = mApplication.getLiveActivity();
        if(liveActivity != null){
            liveActivity.startActivity(intent);
            return;
        }

        throw new NullPointerException("Live activity is null");
    }
}
