package com.manuelemr.mmphotos.presentation.base;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import butterknife.ButterKnife;

/**
 * Created by manuelm on 11/24/16.
 */

public abstract class BaseRecyclerViewHolder<I> extends RecyclerView.ViewHolder {

    public BaseRecyclerViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public abstract void configure(I item);
}
