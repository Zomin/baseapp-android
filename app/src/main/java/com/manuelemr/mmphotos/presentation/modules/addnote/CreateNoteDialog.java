package com.manuelemr.mmphotos.presentation.modules.addnote;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.ApplicationManager;
import com.manuelemr.mmphotos.presentation.base.BaseApplication;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public class CreateNoteDialog extends DialogFragment implements AddNoteView {

    @BindView(R.id.et_title)
    EditText mTitleEditText;
    @BindView(R.id.et_content)
    EditText mContentEditText;

    @Inject
    AddNotePresenter mPresenter;

    private PublishSubject<Note> mNotePublishSubject;
    private PublishSubject<Boolean> mDismissPublishSubject;
    private PublishSubject<NotesViewState> mReportViewStateSubject;

    public CreateNoteDialog() {
        createObservables();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDagger();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout

        View view = inflater.inflate(R.layout.dialog_create_note, null);

        mPresenter.attach(this);

        ButterKnife.bind(this, view);
        builder.setView(view)
                // Add action buttons
                .setPositiveButton(R.string.dialog_ok, (dialog, which) -> {
                    mNotePublishSubject.onNext(
                            new Note(mTitleEditText.getText().toString(), mContentEditText.getText().toString())
                    );
                })
                .setNegativeButton(R.string.dialog_cancel, ((dialog, which) -> {
                    mDismissPublishSubject.onNext(false);
                }));
        return builder.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mPresenter.detach();
    }

    private void injectDagger() {
        ((BaseApplication) getActivity().getApplication()).getAppComponent().inject(this);
    }

    private void createObservables() {

        mReportViewStateSubject = PublishSubject.create();
        mNotePublishSubject = PublishSubject.create();
        mDismissPublishSubject = PublishSubject.create();
    }

    public PublishSubject<NotesViewState> getReportViewStateSubject() {
        return mReportViewStateSubject;
    }

    /** AddNoteView Implementation **/

    @Override
    public Observable<Note> createNote() {
        return mNotePublishSubject;
    }

    @Override
    public Observable<Boolean> dismissDialog() {
        return mDismissPublishSubject;
    }

    @Override
    public void render(NotesViewState state) {

        mReportViewStateSubject.onNext(state);

        //No matter what state, this view will dismiss
        dismiss();

        mReportViewStateSubject = null;
    }

    /** End **/
}
