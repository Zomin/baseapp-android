package com.manuelemr.mmphotos.presentation.base;

import android.app.Application;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.manuelemr.mmphotos.presentation.di.AppComponent;
import com.manuelemr.mmphotos.presentation.di.AppModule;
import com.manuelemr.mmphotos.presentation.di.DaggerAppComponent;
import com.manuelemr.mmphotos.presentation.di.DataModule;
import com.manuelemr.mmphotos.presentation.di.NetModule;

import io.realm.Realm;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class BaseApplication extends Application {

    private AppComponent mAppComponent;

    @Override public void onCreate() {
        super.onCreate();
        ApplicationManager.getInstance().registerActivityLifeCycle(this);
        registerRealm();

        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule())
                .dataModule(new DataModule())
        .build();
    }

    void registerRealm(){
        Realm.init(this);
    }

    @Nullable
    public AppCompatActivity getLiveActivity(){
        return (AppCompatActivity) ApplicationManager.getInstance().getLiveActivity();
    }

    public AppComponent getAppComponent(){
        return mAppComponent;
    }
}
