package com.manuelemr.mmphotos.presentation.modules.notes.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

import java.util.List;

/**
 * Created by manuelmunoz on 7/17/17.
 */

public abstract class NotesViewState {

    private final boolean mIsLoading;
    private final List<Note> mNoteList;
    private final Note mTappedNote;
    private final Note mNewNote;
    private final String mError;

    private final boolean mAddedNewNote;

    protected NotesViewState(boolean isLoading,
                             List<Note> noteList,
                             Note tappedNote,
                             Note newNote,
                             boolean newNoteAdded,
                             String loadingNotesError) {

        mIsLoading = isLoading;
        mNoteList = noteList;
        mTappedNote = tappedNote;
        mNewNote = newNote;
        mAddedNewNote = newNoteAdded;
        mError = loadingNotesError;
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public List<Note> getNoteList() {
        return mNoteList;
    }

    public Note getTappedNote() {
        return mTappedNote;
    }

    public String getLoadingNotesError() {
        return mError;
    }

    public Note getNewNote() {
        return mNewNote;
    }

    public boolean isNewNoteAdded() {
        return mAddedNewNote;
    }
}




