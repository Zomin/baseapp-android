package com.manuelemr.mmphotos.presentation.base;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by manuelm on 11/24/16.
 */

public abstract class BaseRecyclerAdapter<I, VH extends BaseRecyclerViewHolder<I>> extends RecyclerView.Adapter<VH> {

    public interface OnClickListener<I, VH> {
        void onClickItem(I item, VH viewHolder, int position);
    }

    public interface OnLongClickListener<I, VH> {
        /**
         * @return true if the callback consumed the long click, false otherwise.
         */
        boolean onLongClickItem(I item, VH viewHolder, int position);
    }

    protected List<I> mItems = new ArrayList<>();
    protected OnClickListener<I, VH> mOnClickListener;
    protected OnLongClickListener<I, VH> mOnLongClickListener;

    public BaseRecyclerAdapter(){

    }

    //I am hidding 'viewType' on purpose because this adapter only supports single viewTypes
    //If you need an adapter that supports multiple viewTypes extend this one
    public abstract VH createViewHolder(ViewGroup parent, LayoutInflater layoutInflater);

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        return createViewHolder(parent, LayoutInflater.from(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {

        I item = mItems.get(position);
        holder.configure(item);

        if(mOnClickListener != null){
            holder.itemView.setOnClickListener(view -> mOnClickListener.onClickItem(item, holder, holder.getAdapterPosition()));
        }

        if(mOnLongClickListener != null){
            holder.itemView.setOnLongClickListener(view -> mOnLongClickListener.onLongClickItem(item, holder, holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void setOnClickListener(OnClickListener<I, VH> onClickListener) {
        mOnClickListener = onClickListener;
    }

    public void setOnLongClickListener(OnLongClickListener<I, VH> onLongClickListener) {
        mOnLongClickListener = onLongClickListener;
    }

    public void clear(){
        mItems.clear();
        notifyDataSetChanged();
    }

    public void setItems(List<I> items){
        mItems = items;
        notifyDataSetChanged();
    }

    public void addItem(I item){
        mItems.add(item);
        notifyItemInserted(mItems.size());
    }

    public void addItems(List<I> items){
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    public List<I> getItems() {
        return mItems;
    }
}
