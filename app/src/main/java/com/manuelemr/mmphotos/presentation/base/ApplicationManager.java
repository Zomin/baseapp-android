package com.manuelemr.mmphotos.presentation.base;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.support.annotation.Nullable;

import java.lang.ref.WeakReference;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class ApplicationManager {

    private static ApplicationManager sInstance = new ApplicationManager();

    public static ApplicationManager getInstance() {
        return sInstance;
    }

    //Make it a weak reference to avoid possible leaks
    private WeakReference<Activity> mLiveActivity;

    private ApplicationManager() {
    }

    public void registerActivityLifeCycle(Application application){

        application.registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
            @Override public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
                mLiveActivity = new WeakReference<>(activity);
            }

            @Override public void onActivityStarted(Activity activity) {}

            @Override public void onActivityResumed(Activity activity) {
                mLiveActivity = new WeakReference<>(activity);
            }

            @Override public void onActivityPaused(Activity activity) {
                mLiveActivity.clear();
            }

            @Override public void onActivityStopped(Activity activity) {}

            @Override public void onActivitySaveInstanceState(Activity activity, Bundle outState) {}

            @Override public void onActivityDestroyed(Activity activity) {}
        });
    }

    @Nullable
    public Activity getLiveActivity() {
        return mLiveActivity.get();
    }
}
