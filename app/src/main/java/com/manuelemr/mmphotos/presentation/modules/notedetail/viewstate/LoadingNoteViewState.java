package com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate;


/**
 * Created by manuelmunoz on 7/19/17.
 */

public class LoadingNoteViewState extends LoadNoteViewState {

    public LoadingNoteViewState() {
        super(true, null, null, false);
    }

    @Override
    public LoadNoteViewState reduce(LoadNoteViewState previousState) {
        return this;
    }
}
