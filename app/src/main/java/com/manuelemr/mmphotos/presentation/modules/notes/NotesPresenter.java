package com.manuelemr.mmphotos.presentation.modules.notes;

import android.util.Log;

import com.manuelemr.mmphotos.data.base.BasePresenter;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.modules.notes.GetNotesInteractor;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOnMainThread;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.ErrorViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.LoadedNotesViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.LoadingNotesViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;
import com.manuelemr.mmphotos.presentation.navigation.AddNoteNavigator;
import com.manuelemr.mmphotos.presentation.navigation.NoteDetailNavigator;

import java.util.ArrayList;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by manuelmunoz on 2/14/17.
 */

public class NotesPresenter extends BasePresenter<NotesView, NotesViewState> {

    private GetNotesInteractor mGetNotesInteractor;
    private AddNoteNavigator mAddNoteNavigator;
    private NoteDetailNavigator mNoteDetailNavigator;

    @Inject
    public NotesPresenter(NotesViewState notesViewState,
                          ObserveOn observeOn,
                          GetNotesInteractor getNotesInteractor,
                          AddNoteNavigator addNoteNavigator,
                          NoteDetailNavigator noteDetailNavigator,
                          ExceptionParser exceptionParser) {

        super(notesViewState, observeOn, exceptionParser);
        mGetNotesInteractor = getNotesInteractor;
        mAddNoteNavigator = addNoteNavigator;
        mNoteDetailNavigator = noteDetailNavigator;
    }

    @Override
    protected void bindToView() {

        Observable<NotesViewState> loadAllNotes = mView.loadAllNotes()
                .flatMap(ignored -> mGetNotesInteractor.request()
                        .map(LoadedNotesViewState::new)
                        .cast(NotesViewState.class)
                        .onErrorReturn(error -> new ErrorViewState(mExceptionParser.toMessage(error)))
                );


        Observable<NotesViewState> addNote = mView.addNote()
                .flatMap(ignored -> mAddNoteNavigator.navigate()
                        .take(1)
                        .onErrorReturn(error -> new ErrorViewState(mExceptionParser.toMessage(error)))
                );

        mView.toNoteDetail()
                .subscribe(note -> mNoteDetailNavigator.navigate(note));

        Observable<NotesViewState> merged = Observable.merge(loadAllNotes, addNote)
                .observeOn(mObserveOn.getScheduler());

        bindConsumer(merged, viewState -> mView.render(viewState));
    }

    /** End **/
}
