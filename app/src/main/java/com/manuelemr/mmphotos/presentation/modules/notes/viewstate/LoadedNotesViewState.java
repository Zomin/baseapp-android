package com.manuelemr.mmphotos.presentation.modules.notes.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

import java.util.List;

/**
 * Created by manuelmunoz on 7/18/17.
 */

public class LoadedNotesViewState extends NotesViewState {

    public LoadedNotesViewState(List<Note> noteList) {
        super(false, noteList, null, null, false, null);
    }
}
