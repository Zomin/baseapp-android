package com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

/**
 * Created by manuelmunoz on 7/19/17.
 */

public abstract class LoadNoteViewState {

    protected final boolean mIsLoading;
    protected final Note mLoadedNote;
    protected final String mError;
    protected final boolean mIsNoteFavorite;

    protected LoadNoteViewState(boolean isLoading, Note loadedNote, String error, boolean isNoteFavorite) {
        mIsLoading = isLoading;
        mLoadedNote = loadedNote;
        mError = error;
        mIsNoteFavorite = isNoteFavorite;
    }

    public boolean isLoading() {
        return mIsLoading;
    }

    public Note getLoadedNote() {
        return mLoadedNote;
    }

    public String getError() {
        return mError;
    }

    public boolean isNoteFavorite() {
        return mIsNoteFavorite;
    }

    public abstract LoadNoteViewState reduce(LoadNoteViewState previousState);
}
