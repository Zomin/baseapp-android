package com.manuelemr.mmphotos.presentation.modules.addnote;

import com.manuelemr.mmphotos.data.base.BasePresenter;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.modules.addnote.AddNoteInteractor;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOn;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.AddNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.ErrorViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.LoadingNotesViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by manuelmunoz on 2/15/17.
 */

public class AddNotePresenter extends BasePresenter<AddNoteView, NotesViewState> {

    AddNoteInteractor mAddNoteInteractor;

    @Inject
    public AddNotePresenter(ObserveOn observeOn,
                            AddNoteInteractor addNoteInteractor,
                            ExceptionParser exceptionParser) {

        super(null, observeOn, exceptionParser);
        mAddNoteInteractor = addNoteInteractor;
    }

    @Override
    protected void bindToView() {

        Observable<NotesViewState> addNote = mView.createNote()
                .flatMap(note -> {
                    mAddNoteInteractor.setNote(note);
                    return mAddNoteInteractor.request()
                            .map(AddNoteViewState::new)
                            .cast(NotesViewState.class)
                            .onErrorReturn(error -> new ErrorViewState(mExceptionParser.toMessage(error)));
                });


        Observable<NotesViewState> dismissNote = mView.dismissDialog()
                .map(ignored -> new AddNoteViewState(null))
                .cast(NotesViewState.class);

        Observable<NotesViewState> merge = Observable.merge(addNote, dismissNote)
                .observeOn(mObserveOn.getScheduler());

        bindConsumer(merge, viewState -> mView.render(viewState));
    }

    public boolean noteHasEmptyFields(Note note){
        return note.getTitle().isEmpty() || note.getContent().isEmpty();
    }
}
