package com.manuelemr.mmphotos.presentation.modules.notes;

import android.view.View;
import android.widget.TextView;

import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.BaseRecyclerViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class NoteViewHolder extends BaseRecyclerViewHolder<Note> {

    @BindView(R.id.tv_title)
    TextView mTitleTextView;
    @BindView(R.id.tv_content)
    TextView mContentTextView;

    public NoteViewHolder(View itemView) {
        super(itemView);

        ButterKnife.bind(this, itemView);
    }

    @Override
    public void configure(Note item) {

        mTitleTextView.setText(item.getTitle());
        mContentTextView.setText(item.getContent());
    }
}
