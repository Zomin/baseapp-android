package com.manuelemr.mmphotos.presentation.modules.notes.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

import java.util.List;

/**
 * Created by manuelmunoz on 7/17/17.
 */

public class TappedNoteViewState extends NotesViewState {

    public TappedNoteViewState(List<Note> noteList, Note tappedNote) {
        super(false, noteList, tappedNote, null, false, null);
    }
}
