package com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

/**
 * Created by manuelmunoz on 7/23/17.
 */

public class FavoriteNoteViewState extends LoadNoteViewState {

    public FavoriteNoteViewState(boolean isNoteFavorite) {
        super(false, null, null, isNoteFavorite);
    }

    @Override
    public LoadNoteViewState reduce(LoadNoteViewState previousState) {

        if(getLoadedNote() != null){
            Note lodedNote = previousState.getLoadedNote();
            lodedNote.setFavorite(mIsNoteFavorite);
            return new LoadedNoteViewState(lodedNote);
        }

        return this;
    }
}
