package com.manuelemr.mmphotos.presentation.base;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.base.BaseActionListener;
import com.manuelemr.mmphotos.data.base.BasePresenter;
import com.manuelemr.mmphotos.data.base.BaseView;

import javax.inject.Inject;

import butterknife.ButterKnife;

/**
 * Base Fragment class for all fragments in project {@link Fragment} subclass.
 */
public abstract class BaseFragment<P extends BasePresenter> extends Fragment implements BaseView {

    protected MaterialDialog mMaterialDialog;
    protected MaterialDialog mMaterialProgress;
    @Inject
    protected P mPresenter;

    public BaseFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);

        ButterKnife.bind(this, view);
        injectDagger();
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle onSavedInstanceState){
        super.onViewCreated(view, onSavedInstanceState);

        viewCreated(view, onSavedInstanceState);
        mPresenter.attach(this);
    }

    @Override
    public void onDetach(){
        super.onDetach();
        mPresenter.detach();
    }

    protected void showToast(String title) {
        Toast.makeText(getActivity(), title, Toast.LENGTH_LONG)
                .show();
    }

    protected void showDialog(String title, String content){

        mMaterialDialog = new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .positiveText(R.string.dialog_ok)
                .onPositive((MaterialDialog dialog, DialogAction action) ->
                    mMaterialDialog.dismiss()
                )
                .show();
    }

    protected void showLoaderIndeterminate(String title, String content){

        mMaterialProgress = new MaterialDialog.Builder(getContext())
                .title(title)
                .content(content)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    protected void showLoaderIndeterminate(){

        mMaterialProgress = new MaterialDialog.Builder(getContext())
                .progress(true, 0)
                .cancelable(false)
                .show();
    }

    protected void hideLoader(){

        if(mMaterialProgress != null && mMaterialProgress.isShowing()) mMaterialProgress.dismiss();
    }

    @Nullable
    protected BaseApplication getApplication(){

        return (BaseApplication)getActivity().getApplication();
    }

    protected abstract int getLayoutId();
    protected abstract void viewCreated(View view, Bundle savedInstanceState);
    protected abstract void injectDagger();
}
