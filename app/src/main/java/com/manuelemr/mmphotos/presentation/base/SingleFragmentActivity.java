package com.manuelemr.mmphotos.presentation.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.manuelemr.mmphotos.R;

import java.io.Serializable;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SingleFragmentActivity extends AppCompatActivity {

    public interface Specifics {

        String FRAGMENT_BUNDLE_KEY = "fragment_bundle_key";
        String FRAGMENT_KEY = "fragment_key";
        String TITLE_KEY = "title_key";
    }

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_fragment);
        ButterKnife.bind(this);

        setupActionBar();

        setupBackStackListener();
        setup();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void addToBackStack(BaseFragment fragment){

        setupFragment(fragment, true);
    }

    public void popBackStack(){

        getSupportFragmentManager().popBackStack();
    }

    private void setupActionBar(){

        setSupportActionBar(mToolbar);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null){

            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);

            Bundle bundle = getBundleOrThrow();
            String title = bundle.getString(Specifics.TITLE_KEY, "");
            if(!title.isEmpty()){
                actionBar.setTitle(title);
            }
            else {
                actionBar.setTitle(R.string.app_name);
            }
        }
    }

    private Bundle getBundleOrThrow(){
        Bundle extras = getIntent().getExtras();
        if(extras == null) throw new NullPointerException(this.getClass().getSimpleName() +
                " must be started passing a Bundle parameters");

        return extras;
    }

    private void setup(){

        Bundle extras = getBundleOrThrow();

        //Set fragment
        Serializable serializable = extras.getSerializable(Specifics.FRAGMENT_KEY);
        Class<Fragment> clazz = (Class<Fragment>)serializable;
        Fragment fragment = generateFragment(clazz);

        Bundle fragmentBundle = extras.getBundle(Specifics.FRAGMENT_BUNDLE_KEY);

        if(fragmentBundle != null){
            fragment.setArguments(fragmentBundle);
        }

        setupFragment(fragment, false);
    }

    private <T extends Fragment> Fragment generateFragment(Class<T> clazz){

        try {
            return clazz.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException(e.getMessage());
        } catch (IllegalAccessException e) {
            throw new IllegalStateException(e.getMessage());
        }
    }

    private void setupFragment(Fragment fragment, boolean addToBackStack){

        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.fl_container, fragment);

        if(addToBackStack) transaction.addToBackStack(fragment.getClass().getSimpleName());

        transaction.commit();
    }

    private void setupBackStackListener(){

        mToolbar.setNavigationOnClickListener(v -> {
            if(getSupportFragmentManager().getBackStackEntryCount() > 0){
                getSupportFragmentManager().popBackStack();
            }else {
                onBackPressed();
            }
        });
    }
}
