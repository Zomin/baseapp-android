package com.manuelemr.mmphotos.presentation.di;

import com.manuelemr.mmphotos.data.modules.addnote.AddNoteInteractor;
import com.manuelemr.mmphotos.data.modules.favoritenote.FavoriteNoteInteractor;
import com.manuelemr.mmphotos.presentation.modules.addnote.AddNotePresenter;
import com.manuelemr.mmphotos.data.modules.notedetail.GetNoteInteractor;
import com.manuelemr.mmphotos.presentation.modules.notedetail.NoteDetailPresenter;
import com.manuelemr.mmphotos.data.modules.notes.GetNotesInteractor;
import com.manuelemr.mmphotos.presentation.modules.notes.NotesPresenter;
import com.manuelemr.mmphotos.data.services.NotesService;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOn;
import com.manuelemr.mmphotos.data.utils.SubscribeOnMainThread;
import com.manuelemr.mmphotos.presentation.base.BaseApplication;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.LoadingNotesViewState;
import com.manuelemr.mmphotos.presentation.navigation.AddNoteNavigator;
import com.manuelemr.mmphotos.presentation.navigation.NoteDetailNavigator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by manuelmunoz on 2/15/17.
 */

@Module
public class DataModule {

    @Provides
    public NoteDetailNavigator provideNoteDetailNavigator(BaseApplication application){
        return new NoteDetailNavigator(application);
    }

    @Provides
    public AddNoteNavigator provideAddNoteNavigator(BaseApplication application){
        return new AddNoteNavigator(application);
    }

    @Provides
    public ExceptionParser provideExceptionParser(BaseApplication application){
        return new ExceptionParser(application.getApplicationContext());
    }

    @Provides
    @Singleton
    public ObserveOn provideObserveOn(){
        return (AndroidSchedulers::mainThread);
    }

    @Provides
    @Singleton
    public SubscribeOnMainThread provideSubscribeOnMainThread(){
        return new SubscribeOnMainThread();
    }

    @Provides
    @Singleton
    public SubscribeOn provideSubscribeOn(){
        return (Schedulers::io);
    }

    @Provides
    public GetNotesInteractor provideGetNotesInteractor(NotesService notesService){
        return new GetNotesInteractor(notesService);
    }

    @Provides
    AddNoteInteractor provideAddNoteInteractor(NotesService notesService){
        return new AddNoteInteractor(notesService);
    }

    @Provides
    GetNoteInteractor provideGetNoteInteractor(NotesService notesService){
        return new GetNoteInteractor(notesService);
    }

    //Notes module
    @Provides
    NotesPresenter provideNotesPresenter(ObserveOn observeOn,
                                         GetNotesInteractor getNotesInteractor,
                                         AddNoteNavigator addNoteNavigator,
                                         NoteDetailNavigator noteDetailNavigator,
                                         ExceptionParser exceptionParser){

        return new NotesPresenter(new LoadingNotesViewState(),
                observeOn,
                getNotesInteractor,
                addNoteNavigator,
                noteDetailNavigator,
                exceptionParser);
    }

    @Provides
    AddNotePresenter provideAddNotePresenter(ObserveOn observeOn,
                                             AddNoteInteractor addNoteInteractor,
                                             ExceptionParser exceptionParser){

        return new AddNotePresenter(observeOn, addNoteInteractor, exceptionParser);
    }

    @Provides
    NoteDetailPresenter provideNoteDetailPresenter(ObserveOn observeOn,
                                                        ExceptionParser exceptionParser,
                                                        FavoriteNoteInteractor favoriteNoteInteractor,
                                                        GetNoteInteractor getNoteInteractor){

        return new NoteDetailPresenter(observeOn, getNoteInteractor, favoriteNoteInteractor, exceptionParser);
    }


}
