package com.manuelemr.mmphotos.presentation.di;

import com.manuelemr.mmphotos.presentation.modules.addnote.CreateNoteDialog;
import com.manuelemr.mmphotos.presentation.modules.notedetail.NoteDetailFragment;
import com.manuelemr.mmphotos.presentation.modules.notes.NotesFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by manuelmunoz on 2/15/17.
 */

@Singleton
@Component(modules = {AppModule.class, NetModule.class, DataModule.class})
public interface AppComponent {

    void inject(NotesFragment notesFragment);
    void inject(NoteDetailFragment noteDetailFragment);
    void inject(CreateNoteDialog createNoteDialog);
}
