package com.manuelemr.mmphotos.presentation.modules.notedetail;

import com.manuelemr.mmphotos.data.base.BasePresenter;
import com.manuelemr.mmphotos.data.modules.favoritenote.FavoriteNoteInteractor;
import com.manuelemr.mmphotos.data.modules.notedetail.GetNoteInteractor;
import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.FavoriteNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadedNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadingNoteErrorViewState;
import com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate.LoadingNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.AddNoteViewState;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.internal.operators.observable.ObservableAll;


/**
 * Created by manuelmunoz on 2/15/17.
 */

public class NoteDetailPresenter extends BasePresenter<NoteDetailView, LoadNoteViewState> {

    GetNoteInteractor mGetNoteInteractor;
    FavoriteNoteInteractor mFavoriteNoteInteractor;

    @Inject
    public NoteDetailPresenter(ObserveOn observeOn,
                               GetNoteInteractor getNoteInteractor,
                               FavoriteNoteInteractor favoriteNoteInteractor,
                               ExceptionParser exceptionParser) {

        super(new LoadingNoteViewState(), observeOn, exceptionParser);
        mGetNoteInteractor = getNoteInteractor;
        mFavoriteNoteInteractor = favoriteNoteInteractor;
    }

    @Override
    protected void bindToView() {

        Observable<LoadNoteViewState> loadNote = mView.loadNote()
                .flatMap(id -> {
                    mGetNoteInteractor.setNoteId(id);
                    return mGetNoteInteractor.request()
                            .map(LoadedNoteViewState::new)
                            .cast(LoadNoteViewState.class)
                            .onErrorReturn(error -> new LoadingNoteErrorViewState(mExceptionParser.toMessage(error)));
                });


        Observable<LoadNoteViewState> favoriteNote = mView.favoriteNote()
                .flatMap(id -> {
                    mFavoriteNoteInteractor.setNoteId(id);
                    return mFavoriteNoteInteractor.request()
                            .map(FavoriteNoteViewState::new)
                            .cast(LoadNoteViewState.class)
                            .onErrorReturn(error -> new LoadingNoteErrorViewState(mExceptionParser.toMessage(error)));
                });

        Observable<LoadNoteViewState> merge = Observable.merge(loadNote, favoriteNote)
                .observeOn(mObserveOn.getScheduler());

        Observable<LoadNoteViewState> reducedState = merge.scan(
                (loadNoteViewState, loadNoteViewState2) -> loadNoteViewState2.reduce(loadNoteViewState));

        bindConsumer(reducedState, viewState -> mView.render(viewState));
    }
}
