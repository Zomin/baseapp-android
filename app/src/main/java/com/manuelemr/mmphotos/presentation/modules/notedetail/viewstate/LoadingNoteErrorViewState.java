package com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate;


import com.manuelemr.mmphotos.data.models.Note;

/**
 * Created by manuelmunoz on 7/19/17.
 */

public class LoadingNoteErrorViewState extends LoadNoteViewState {

    public LoadingNoteErrorViewState(String error) {
        super(false, null, error, false);
    }

    @Override
    public LoadNoteViewState reduce(LoadNoteViewState previousStatechanges) {
        return this;
    }
}
