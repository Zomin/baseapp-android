package com.manuelemr.mmphotos.presentation.modules.notes;

import com.manuelemr.mmphotos.data.base.BaseView;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import io.reactivex.Observable;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public interface NotesView extends BaseView {

    Observable<Boolean> loadAllNotes();
    Observable<Object> addNote();
    Observable<Note> toNoteDetail();

    void render(NotesViewState viewState);
}
