package com.manuelemr.mmphotos.presentation.modules.notedetail.viewstate;

import com.manuelemr.mmphotos.data.models.Note;

/**
 * Created by manuelmunoz on 7/19/17.
 */

public class LoadedNoteViewState extends LoadNoteViewState {

    public LoadedNoteViewState(Note loadedNote) {
        super(false, loadedNote, null, loadedNote.isFavorite());
    }

    @Override
    public LoadNoteViewState reduce(LoadNoteViewState previousState) {
        return this;
    }
}
