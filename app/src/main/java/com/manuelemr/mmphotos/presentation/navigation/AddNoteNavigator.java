package com.manuelemr.mmphotos.presentation.navigation;

import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.presentation.base.BaseApplication;
import com.manuelemr.mmphotos.presentation.modules.addnote.CreateNoteDialog;
import com.manuelemr.mmphotos.presentation.modules.notes.viewstate.NotesViewState;

import io.reactivex.Observable;

/**
 * Created by manuelmunoz on 7/18/17.
 */

public class AddNoteNavigator extends Navigator {

    public AddNoteNavigator(BaseApplication application) {
        super(application);
    }

    public Observable<NotesViewState> navigate(){

        CreateNoteDialog dialog = new CreateNoteDialog();
        dialog.show(getLiveActivity().getSupportFragmentManager(), dialog.getClass().getSimpleName());

        return dialog.getReportViewStateSubject();
    }
}
