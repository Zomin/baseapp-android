package com.manuelemr.mmphotos.presentation.di;

import com.manuelemr.mmphotos.data.services.NotesService;
import com.manuelemr.mmphotos.data.services.NotesServiceImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by manuelmunoz on 2/15/17.
 */

@Module
public class NetModule {

    @Provides
    @Singleton
    public NotesService provideNotesService(){
        return new NotesServiceImpl();
    }
}
