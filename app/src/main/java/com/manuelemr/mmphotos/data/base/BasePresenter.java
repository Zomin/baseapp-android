package com.manuelemr.mmphotos.data.base;

import android.support.annotation.Nullable;

import com.manuelemr.mmphotos.data.utils.ExceptionParser;
import com.manuelemr.mmphotos.data.utils.ObserveOn;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public abstract class BasePresenter<V extends BaseView, VS> {

    protected interface ViewStateConsumer<VS> {
        void render(VS viewState);
    }

    protected V mView;
    protected ObserveOn mObserveOn;
    protected ExceptionParser mExceptionParser;
    protected BehaviorSubject<VS> mViewStateBehaviourSubject;

    private CompositeDisposable mCompositeDisposable;

    public BasePresenter(VS mInitialViewState, ObserveOn observeOn, ExceptionParser exceptionParser){

        mObserveOn = observeOn;
        mExceptionParser = exceptionParser;
        mCompositeDisposable = new CompositeDisposable();
        if(mInitialViewState == null) {
            mViewStateBehaviourSubject = BehaviorSubject.create();
        } else {
            mViewStateBehaviourSubject = BehaviorSubject.createDefault(mInitialViewState);
        }
    }

    protected abstract void bindToView();

    /**
     * Must be called on child bindToView
     * @param viewStateConsumer
     */
    protected void bindConsumer(Observable<VS> observable, ViewStateConsumer<VS> viewStateConsumer){
        observable.subscribe(mViewStateBehaviourSubject);
        mCompositeDisposable.add(mViewStateBehaviourSubject.subscribe(viewStateConsumer::render));
    }

    public void attach(V view){
        mView = view;
        bindToView();
    }

    public void detach() {

        mView = null;
        if(!mCompositeDisposable.isDisposed()){
            mCompositeDisposable.dispose();
        }
    }

    @Nullable
    public VS getViewState(){
        return mViewStateBehaviourSubject.getValue();
    }
}
