package com.manuelemr.mmphotos.data.modules.notedetail;

import com.manuelemr.mmphotos.data.base.BaseInteractor;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.services.NotesService;

import javax.inject.Inject;

import io.reactivex.Observable;


/**
 * Created by manuelmunoz on 2/15/17.
 */

public class GetNoteInteractor extends BaseInteractor<Note, NotesService> {

    private int mNoteId;

    @Inject
    public GetNoteInteractor(NotesService service) {
        super(service);
    }

    public void setNoteId(int noteId){
        mNoteId = noteId;
    }

    @Override
    public Observable<Note> request() {
        return mService.getNote(mNoteId);
    }
}
