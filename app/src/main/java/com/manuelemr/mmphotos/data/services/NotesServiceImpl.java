package com.manuelemr.mmphotos.data.services;

import com.manuelemr.mmphotos.data.exceptions.NoteNotFoundException;
import com.manuelemr.mmphotos.data.models.Note;

import java.util.ArrayList;
import java.util.List;

import hu.akarnokd.rxjava.interop.RxJavaInterop;
import io.reactivex.Observable;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmModel;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class NotesServiceImpl implements NotesService {

    public NotesServiceImpl(){
    }

    private int getNextNoteId(){

        Realm realm = Realm.getDefaultInstance();
        Number maxValue = realm.where(Note.class).max(Note.ID);
        if(maxValue == null) {
            realm.close();
            return  0;
        }

        realm.close();
        return maxValue.intValue() + 1;
    }

    /** Notes Service Implementation **/

    @Override
    public Observable<Note> addNote(Note note) {



        return Observable.create(subscriber -> {
            note.setId(getNextNoteId());

            Realm realm = Realm.getDefaultInstance();
            realm.executeTransactionAsync(
                    localRealm -> localRealm.copyToRealm(note),
                    () -> {
                        subscriber.onNext(note);
                        subscriber.onComplete();
                        realm.close();
                    },
                    error -> {
                        subscriber.onError(error);
                        realm.close();
                    }
            );
        });
    }

    @Override
    public Observable<List<Note>> getAllNotes() {

        Realm realm = Realm.getDefaultInstance();

        Observable<List<Note>> responseNotes = RxJavaInterop
                .toV2Observable(realm.where(Note.class)
                        .findAllAsync()
                        .asObservable()
                )
                .doFinally(realm::close)
                .map(ArrayList::new);

        return responseNotes;
    }

    @Override
    public Observable<Note> getNote(int id){

        Realm realm = Realm.getDefaultInstance();

        return RxJavaInterop.toV2Observable(
                realm.where(Note.class).equalTo(Note.ID, id).findFirstAsync().asObservable())
                .cast(Note.class)
                .filter(realmObject -> realmObject.isLoaded())
                .doFinally(realm::close);
    }

    @Override
    public Observable<Boolean> favoriteNote(int noteId) {

        Realm realm = Realm.getDefaultInstance();

        return Observable.create(subscriber ->
                realm.executeTransactionAsync(
                        localRealm -> {

                            //When updating note instance here, Loaded note from 'getNote' gets automatically updated
                            //Realm isntances are 'live instances', no need to update throu observable
                            Note note = localRealm.where(Note.class).equalTo(Note.ID, noteId).findFirst();
                            note.setFavorite(!note.isFavorite());
                            subscriber.onNext(note.isFavorite());
                        },
                        subscriber::onComplete,
                        subscriber::onError))
                .cast(Boolean.class)
                .doFinally(() -> realm.close());
    }

    /** End **/
}
