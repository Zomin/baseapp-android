package com.manuelemr.mmphotos.data.modules.notes;

import com.manuelemr.mmphotos.data.base.BaseInteractor;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.services.NotesService;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by manuelmunoz on 2/14/17.
 */

public class GetNotesInteractor extends BaseInteractor<List<Note>, NotesService> {

    public GetNotesInteractor(NotesService service) {
        super(service);
    }

    @Override
    public Observable<List<Note>> request() {
        return mService.getAllNotes();
    }
}
