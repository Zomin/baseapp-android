package com.manuelemr.mmphotos.data.utils;


import io.reactivex.Scheduler;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public interface ObserveOn {

    Scheduler getScheduler();
}
