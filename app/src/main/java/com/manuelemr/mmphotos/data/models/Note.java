package com.manuelemr.mmphotos.data.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public class Note extends RealmObject{

    public static final String ID = "mId";

    @PrimaryKey
    private int mId;
    private String mTitle;
    private String mContent;
    private boolean mIsFavorite = false;

    public Note(){}

    public Note(String title, String content){

        mTitle = title;
        mContent = content;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getContent() {
        return mContent;
    }

    public void setContent(String content) {
        mContent = content;
    }

    public boolean isFavorite() {
        return mIsFavorite;
    }

    public void setFavorite(boolean favorite) {
        mIsFavorite = favorite;
    }
}
