package com.manuelemr.mmphotos.data.exceptions;

import com.manuelemr.mmphotos.data.models.Note;

/**
 * Created by manuelmunoz on 2/15/17.
 */
public class NoteNotFoundException extends Exception {

    String format = "Tried to find note with %i but found null";

    public NoteNotFoundException(int noteId) {
        initCause(new Throwable(String.format(format, noteId)));
    }

    public NoteNotFoundException(Throwable cause) {
        super(cause);
    }

    public NoteNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoteNotFoundException(String message) {
        super(message);
    }
}
