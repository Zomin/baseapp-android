package com.manuelemr.mmphotos.data.base;

/**
 * Created by manuelmunoz on 2/14/17.
 */

public interface BaseActionListener<V> {

    void attach(V view);
    void detach();
}
