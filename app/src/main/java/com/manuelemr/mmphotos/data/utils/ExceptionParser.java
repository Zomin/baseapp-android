package com.manuelemr.mmphotos.data.utils;

import android.content.Context;

import com.manuelemr.mmphotos.R;
import com.manuelemr.mmphotos.data.exceptions.*;

import java.lang.ref.WeakReference;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public class ExceptionParser {

    private WeakReference<Context> mContext;

    public ExceptionParser(Context context){
        mContext = new WeakReference<>(context);
    }

    public String toMessage(Throwable t){

        Context context = mContext.get();
        if(context == null) throw new NullPointerException("Context is null");

        if(t instanceof NoteNotFoundException){
            return context.getString(R.string.note_empty);
        }

        return t.getLocalizedMessage();
    }
}
