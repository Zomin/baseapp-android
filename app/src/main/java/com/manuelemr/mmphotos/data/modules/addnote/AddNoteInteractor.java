package com.manuelemr.mmphotos.data.modules.addnote;

import com.manuelemr.mmphotos.data.base.BaseInteractor;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.services.NotesService;

import io.reactivex.Observable;


/**
 * Created by manuelmunoz on 2/15/17.
 */

public class AddNoteInteractor extends BaseInteractor<Note, NotesService> {

    private Note mNote;

    public AddNoteInteractor(NotesService service) {
        super(service);
    }

    public void setNote(Note note){
        mNote = note;
    }

    @Override
    public Observable<Note> request() {

        if(mNote == null) throw new NullPointerException("mNote must not be null");

        return mService.addNote(mNote);
    }
}
