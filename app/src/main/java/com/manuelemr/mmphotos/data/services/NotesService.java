package com.manuelemr.mmphotos.data.services;

import com.manuelemr.mmphotos.data.base.BaseService;
import com.manuelemr.mmphotos.data.models.Note;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by manuelmunoz on 2/14/17.
 */

public interface NotesService extends BaseService {

    Observable<Note> addNote(Note note);
    Observable<List<Note>> getAllNotes();
    Observable<Note> getNote(int id);
    Observable<Boolean> favoriteNote(int noteId);
}
