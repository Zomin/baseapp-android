package com.manuelemr.mmphotos.data.base;

/**
 * Created by manuelmunoz on 2/14/17.
 */


import io.reactivex.Observable;

/**
 * Base class for all interactors. This class handles business logic or 'UseCases'.
 * @param <I> Represents the type of data that must return to the presenter
 * @param <S> Represents the type of the service, from where to fetch the data
 */
public abstract  class BaseInteractor<I, S extends BaseService> {

    protected S mService;

    public BaseInteractor(S service){

        mService = service;
    }

    public abstract Observable<I> request();
}
