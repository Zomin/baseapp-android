package com.manuelemr.mmphotos.data.utils;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by manuelmunoz on 2/15/17.
 */

public class SubscribeOnMainThread implements SubscribeOn {
    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
