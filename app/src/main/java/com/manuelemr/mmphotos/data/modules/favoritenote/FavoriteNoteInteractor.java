package com.manuelemr.mmphotos.data.modules.favoritenote;

import com.manuelemr.mmphotos.data.base.BaseInteractor;
import com.manuelemr.mmphotos.data.models.Note;
import com.manuelemr.mmphotos.data.services.NotesService;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by manuelmunoz on 7/19/17.
 */

public class FavoriteNoteInteractor extends BaseInteractor<Boolean, NotesService> {

    private static final int NO_ID = -1020;
    private int mNoteId = -NO_ID;

    @Inject
    public FavoriteNoteInteractor(NotesService service) {
        super(service);
    }

    public void setNoteId(int noteId) {
        mNoteId = noteId;
    }

    @Override
    public Observable<Boolean> request() {

        if(mNoteId == NO_ID){
            throw new NullPointerException("Note id must not be null");
        }

        return mService.favoriteNote(mNoteId);
    }


}
